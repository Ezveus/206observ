def med(n, values, number):
    j = 0
    for v in number:
        i = 0
        while i < v:
            values.append(values[j])
            i += 1
        j += 1
    values = sorted(values)
    n = len(values)
    if n % 2 == 0:
        p = int(n / 2)
        p2 = p + 1
        return ((values[p] + values[p2]) / 2)
    p = int((n + 1) / 2)
    return values[p]

def mod(n, values, number):
    max = 0
    maxi = 0
    i = 0
    while i < n:
        if number[i] > max:
            max = number[i]
            maxi = i
        i += 1
    return values[maxi]
