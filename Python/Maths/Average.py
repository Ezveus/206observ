import math

def arith(n, values, number):
    i = 0
    num = 0
    denom = 0
    while i < n:
        num += (values[i] * number[i])
        denom += number[i]
        i += 1
    return (num / denom)

def quadra(n, values, number):
    i = 0
    valuesSquared = []
    div = 0
    while i < n:
        valuesSquared.append(math.pow(values[i], 2) * number[i])
        div += number[i]
        i += 1
    sum = 0
    div = 1 / div
    for v in valuesSquared:
        sum += v
    return math.sqrt(div * sum)

def geom(n, values, number):
    vsum = 0
    i = 0
    for v in values:
        vsum += math.log(v) * number[i]
        i += 1
    nsum = 0
    for v in number:
        nsum += v
    res = (1 / nsum) * vsum
    return math.exp(res)

def harm(n, values, number):
    nsum = 0
    for v in number:
        nsum += v
    i = 0
    vsum = 0
    for v in values:
        vsum += ((1 / v) * number[i])
        i += 1
    return (nsum / vsum)
