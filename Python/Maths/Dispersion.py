import math
import Maths.Average as Average
import Maths.Stats as Stats

def var(n, values, number):
    return (Average.arith(n, [v ** 2 for v in values], number) - \
                (Average.arith(n, values, number) ** 2))

def typ(n, values, number):
    return (math.sqrt(var(n, values, number)))

def avg(n, values, number):
    avg_ = Average.arith(n, values,number)
    return (Average.arith(n, [math.fabs(v - avg_) for v in values], number))

def med(n, values, number):
    med_ = Stats.med(n, values,number)
    return (Average.arith(n, [math.fabs(v - med_) for v in values], number))
