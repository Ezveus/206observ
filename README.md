# 206observ #

This is the 6th project for the Maths module of my 2nd year in Epitech.

### Goals of the project ###

* Read the input file
* Implement a number of math algorithms:

	- Arithmetic mean
	- Root mean square
	- Geometric mean
	- Harmonic mean
	- Median
	- Mode
	- Variance
	- Standard deviation
	- Mean absolute difference
	- Median absolute difference

### Languages ###

This project is one of the few I wrote in Python (3) and one of my first Ruby scripts.
